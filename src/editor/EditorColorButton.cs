using Godot;
using System;

public class EditorColorButton : Button
{

	public static EditorColorButton Instance()
	{
		return GD.Load<PackedScene>("res://src/editor/EditorColorButton.tscn").Instance() as EditorColorButton;
	}

	private Texture texture = null;

	public Texture Texture
	{
		get
		{
			return texture;
		}
		set
		{
			texture = value;
			
			if (rect != null)
			{
				rect.Texture = value;
			}
		}
	} 

	private TextureRect rect;

    public override void _Ready()
    {
		rect = GetNode<TextureRect>("TextureRect");
		Texture = texture;
	}
}
