using Godot;
using Godot.Collections;
using System;

public class Editor : Spatial
{
	enum OptionalToggle { Ignore, Yes, No };

	private const float RayLength = 1000;

	private ButtonGroup TerrainButtonGroup = new ButtonGroup();
	private ButtonGroup RiverButtonGroup = new ButtonGroup();
	private ButtonGroup WallButtonGroup = new ButtonGroup();
	private ButtonGroup RoadButtonGroup = new ButtonGroup();

	private Material terrainMaterial = GD.Load<Material>("res://graphics/materials/terrain_mat.tres");

	[Export] private string[] terrainTypes = new string[0];

	[Export] private int minElevation = -10;
	[Export] private int maxElevation = 10;
	[Export] private int maxBrushSize = 5;

	private int activeBrushSize;
	private int activeElevation;
	private int activeWaterLevel;

	private int activeUrbanLevel, activeFarmLevel, activePlantLevel;


	private int activeTerrainType;


	private bool applyElevation = true;
	private bool applyWaterLevel = true;
	private bool additiveElevation = false;

	private bool applyUrbanLevel, applyFarmLevel, applyPlantLevel = false;
	private bool applyTerrain = true;
	private bool showGrid = false;

	private HexGrid grid;

	private HSlider brushSizeSlider;

	private CheckBox elevationCheckBox;
	private HSlider elevationSlider;
	private CheckBox additiveElevationCheckBox;

	private CheckBox waterLevelCheckBox;
	private HSlider waterLevelSlider;

	private CheckBox urbanLevelCheckBox;
	private HSlider urbanLevelSlider;

	private CheckBox farmLevelCheckBox;
	private HSlider farmLevelSlider;

	private CheckBox plantLevelCheckBox;
	private HSlider plantLevelSlider;

	private CheckBox terrainCheckBox;

	private CheckBox riverIgnoreCheckBox;
	private CheckBox riverYesCheckBox;
	private CheckBox riverNoCheckBox;

	private CheckBox roadIgnoreCheckBox;
	private CheckBox roadYesCheckBox;
	private CheckBox roadNoCheckBox;

	private CheckBox wallIgnoreCheckBox;
	private CheckBox wallYesCheckBox;
	private CheckBox wallNoCheckBox;
	
	private CheckBox showGridCheckBox;

	private OptionalToggle riverMode = OptionalToggle.Ignore;
	private OptionalToggle roadMode = OptionalToggle.Ignore;
	private OptionalToggle walledMode = OptionalToggle.Ignore;

	private GridContainer terrainButtons;

	private HexCell previousCell, searchFromCell = null;

	private bool isDrag = false;

	private HexMetrics.Direction dragDirection;

	public override void _Ready()
	{
		grid = GetNode<HexGrid>("HexGrid");

		SetupElevation();
		SetupBrush();
		SetupTerrain();

		SetupRiver();
		SetupWall();
		SetupWater();
		SetupUrban();
		SetupFarm();
		SetupPlant();

		SetupRoad();

		Randomize();

	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (@event is InputEventMouseButton)
		{
			HandleInput();
		}
		else if (@event is InputEventMouseMotion)
		{
			HandleInput();
		}
		else
		{
			previousCell = null;
		}
	}

	private void HandleInput()
	{
		var result = ShootRay();

		if (result.Contains("position"))
		{
			var cell = grid.GetCell((Vector3)result["position"]);

			grid.ResetVisibility();
			grid.IncreaseVisibility(cell, activeBrushSize);
			
			if (cell != previousCell && Input.IsMouseButtonPressed((int)Godot.ButtonList.Left))
			{
				if (previousCell != null && previousCell != cell)
				{
					ValidateDrag(cell);
				}

				else
				{
					isDrag = false;
				}

				EditCells(cell);
				
				if (cell != null && searchFromCell != null)
				{
					// grid.FindPath(searchFromCell, cell, 5);
				}

				searchFromCell = cell;
				previousCell = cell;
			}
		}
		else
		{
			previousCell = null;
		}
	}

	private Dictionary ShootRay()
	{
		var spaceState = GetWorld().DirectSpaceState;
		var camera = GetNode("Camera/Camera") as Camera;

		var from = camera.ProjectRayOrigin(GetViewport().GetMousePosition());
		var to = from + camera.ProjectRayNormal(GetViewport().GetMousePosition()) * RayLength;

		return spaceState.IntersectRay(from, to, new Godot.Collections.Array() { this }); ;
	}

	private void EditCells(HexCell center)
	{
		previousCell = center;

		int centerX = center.Coordinates.X;
		int centerZ = center.Coordinates.Z;

		for (int r = 0, z = centerZ - activeBrushSize; z <= centerZ; z++, r++)
		{
			for (int x = centerX - r; x <= centerX + activeBrushSize; x++)
			{
				EditCell(grid.GetCell(new Coordinates(x, z)));
			}
		}

		for (int r = 0, z = centerZ + activeBrushSize; z > centerZ; z--, r++)
		{
			for (int x = centerX - activeBrushSize; x <= centerX + r; x++)
			{
				EditCell(grid.GetCell(new Coordinates(x, z)));
			}
		}
	}

	private void EditCell(HexCell cell)
	{
		if (cell == null)
		{
			return;
		}

		if (applyTerrain)
		{
			cell.TerrainTypeIndex = activeTerrainType;
		}

		if (applyElevation)
		{
			if (additiveElevation)
			{
				cell.Elevation += activeElevation;
			}
			else
			{
				cell.Elevation = activeElevation;
			}
		}

		if (applyWaterLevel)
		{
			cell.WaterLevel = activeWaterLevel;
		}

		if (applyUrbanLevel)
		{
			cell.UrbanLevel = activeUrbanLevel;
		}

		if (applyFarmLevel)
		{
			cell.FarmLevel = activeFarmLevel;
		}

		if (applyPlantLevel)
		{
			cell.PlantLevel = activePlantLevel;
		}

		if (riverMode == OptionalToggle.No)
		{
			cell.RemoveRiver();
		}

		if (roadMode == OptionalToggle.No)
		{
			cell.RemoveRoads();
		}

		if (walledMode != OptionalToggle.Ignore)
		{
			cell.Walled = walledMode == OptionalToggle.Yes;
		}

		if (isDrag)
		{
			HexCell otherCell = cell.GetNeighbor(dragDirection.Opposite());

			if (otherCell != null)
			{
				if (riverMode == OptionalToggle.Yes)
				{
					otherCell.SetOutgoingRiver(dragDirection);
				}
				if (roadMode == OptionalToggle.Yes)
				{
					otherCell.AddRoad(dragDirection);
				}
			}
		}

		cell.Refresh();
	}

	private void ValidateDrag(HexCell currentCell)
	{
		for (dragDirection = HexMetrics.Direction.NE; dragDirection <= HexMetrics.Direction.NW; dragDirection++)
		{
			if (previousCell.GetNeighbor(dragDirection) == currentCell)
			{
				isDrag = true;
				return;
			}
		}
		isDrag = false;
	}

	private void Randomize()
	{
		GD.Randomize();

		for (int i = 0; i < grid.Width * grid.Height; i++)
		{
			long r = GD.Randi() % terrainTypes.Length - 1;
			// GD.Print(r);
			
			var cell = grid.GetCell(i);

			cell.TerrainTypeIndex = (int) r + 1;
			cell.Elevation = (int) r;
			cell.WaterLevel = 0;
		}

		grid.Refresh();
	}

	private void SetAdditiveElevation(bool value)
	{
		additiveElevation = value;
	}

	private void SetApplyTerrain(bool value)
	{
		applyTerrain = value;
	}

	private void SetApplyElevation(bool value)
	{
		applyElevation = value;
	}

	private void SetBrushSize(int size)
	{
		activeBrushSize = size;
	}

	private void SetTerrain(int index)
	{
		activeTerrainType = index;
	}

	private void SetElevation(int elevation)
	{
		activeElevation = elevation;
	}

	public void SetApplyWaterLevel(bool value)
	{
		applyWaterLevel = value;
	}

	public void SetWaterLevel(float level)
	{
		activeWaterLevel = (int)level;
	}

	public void SetApplyUrbanLevel(bool value)
	{
		applyUrbanLevel = value;
	}

	public void SetApplyFarmLevel(bool value)
	{
		applyFarmLevel = value;
	}

	public void SetApplyPlantLevel(bool value)
	{
		applyPlantLevel = value;
	}

	public void SetUrbanLevel(float level)
	{
		activeUrbanLevel = (int)level;
	}

	public void SetFarmLevel(float level)
	{
		activeFarmLevel = (int)level;
	}

	public void SetPlantLevel(float level)
	{
		activePlantLevel = (int)level;
	}

	private void SetRiverMode(OptionalToggle mode)
	{
		riverMode = mode;
	}

	private void SetRoadMode(OptionalToggle mode)
	{
		roadMode = mode;
	}

	private void SetWallMode(OptionalToggle mode)
	{
		walledMode = mode;
	}

	private void ToggleGrid(bool toggle)
	{
		showGrid = toggle;
		terrainMaterial.Set("shader_param/show_grid", showGrid);

	}

	private void SetupRiver()
	{
		riverIgnoreCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/River/VBoxContainer/Ignore");
		riverIgnoreCheckBox.Connect("pressed", this, "SetRiverMode", new Godot.Collections.Array() { OptionalToggle.Ignore });
		riverIgnoreCheckBox.Group = RiverButtonGroup;

		riverYesCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/River/VBoxContainer/Yes");
		riverYesCheckBox.Connect("pressed", this, "SetRiverMode", new Godot.Collections.Array() { OptionalToggle.Yes });
		riverYesCheckBox.Group = RiverButtonGroup;

		riverNoCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/River/VBoxContainer/No");
		riverNoCheckBox.Connect("pressed", this, "SetRiverMode", new Godot.Collections.Array() { OptionalToggle.No });
		riverNoCheckBox.Group = RiverButtonGroup;

		SetRiverMode(OptionalToggle.Ignore);
	}

	private void SetupWall()
	{
		wallIgnoreCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Wall/VBoxContainer/Ignore");
		wallIgnoreCheckBox.Connect("pressed", this, "SetWallMode", new Godot.Collections.Array() { OptionalToggle.Ignore });
		wallIgnoreCheckBox.Group = WallButtonGroup;

		wallYesCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Wall/VBoxContainer/Yes");
		wallYesCheckBox.Connect("pressed", this, "SetWallMode", new Godot.Collections.Array() { OptionalToggle.Yes });
		wallYesCheckBox.Group = WallButtonGroup;

		wallNoCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Wall/VBoxContainer/No");
		wallNoCheckBox.Connect("pressed", this, "SetWallMode", new Godot.Collections.Array() { OptionalToggle.No });
		wallNoCheckBox.Group = WallButtonGroup;

		SetWallMode(OptionalToggle.Ignore);
	}


	private void SetupRoad()
	{
		roadIgnoreCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Road/VBoxContainer/Ignore");
		roadIgnoreCheckBox.Connect("pressed", this, "SetRoadMode", new Godot.Collections.Array() { OptionalToggle.Ignore });
		roadIgnoreCheckBox.Group = RoadButtonGroup;

		roadYesCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Road/VBoxContainer/Yes");
		roadYesCheckBox.Connect("pressed", this, "SetRoadMode", new Godot.Collections.Array() { OptionalToggle.Yes });
		roadYesCheckBox.Group = RoadButtonGroup;

		roadNoCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/Road/VBoxContainer/No");
		roadNoCheckBox.Connect("pressed", this, "SetRoadMode", new Godot.Collections.Array() { OptionalToggle.No });
		roadNoCheckBox.Group = RoadButtonGroup;

		SetRoadMode(OptionalToggle.Ignore);
	}

	private void SetupTerrain()
	{
		
		terrainButtons = GetNode<GridContainer>("CanvasLayer/Panel/Options/TerrainButtons");

		terrainCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyTerrain");
		terrainCheckBox.Connect("toggled", this, "SetApplyTerrain");
		
		showGridCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ShowGrid");
		showGridCheckBox.Connect("toggled", this, "ToggleGrid");

		Data data = GetNode<Data>("/root/Data");
		Texture[] textures = data.GetTextures();

		for (int i = 0; i < textures.Length; i++)
		{
			var tex = textures[i];
			var button = EditorColorButton.Instance();
			button.Group = TerrainButtonGroup;
			button.FocusMode = BaseButton.FocusModeEnum.None;
			button.ToggleMode = true;
			button.Texture = tex;
			terrainButtons.AddChild(button);
			button.Connect("pressed", this, "SetTerrain", new Godot.Collections.Array { button.GetIndex() });
		}

		SetApplyTerrain(terrainCheckBox.Pressed);
		SetTerrain(0);
	}

	private void SetupBrush()
	{
		brushSizeSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/BrushSize");
		brushSizeSlider.Connect("value_changed", this, "SetBrushSize");

		brushSizeSlider.MinValue = 0;
		brushSizeSlider.MaxValue = maxBrushSize;
		brushSizeSlider.Value = 0;
		brushSizeSlider.Step = 1;

		SetBrushSize(0);
	}

	private void SetupWater()
	{
		waterLevelSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/WaterLevelSlider");
		waterLevelSlider.Connect("value_changed", this, "SetWaterLevel");

		waterLevelSlider.MinValue = 0;
		waterLevelSlider.MaxValue = maxElevation;
		waterLevelSlider.Value = 0;
		waterLevelSlider.Step = 1;

		waterLevelCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyWaterLevel");
		waterLevelCheckBox.Connect("toggled", this, "SetApplyWaterLevel");

		SetApplyWaterLevel(waterLevelCheckBox.Pressed);
		SetWaterLevel(0);
	}

	private void SetupUrban()
	{
		urbanLevelSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/UrbanLevelSlider");
		urbanLevelSlider.Connect("value_changed", this, "SetUrbanLevel");

		urbanLevelSlider.MinValue = 0;
		urbanLevelSlider.MaxValue = 3;
		urbanLevelSlider.Value = 0;
		urbanLevelSlider.Step = 1;

		urbanLevelCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyUrbanLevel");
		urbanLevelCheckBox.Connect("toggled", this, "SetApplyUrbanLevel");

		SetApplyUrbanLevel(urbanLevelCheckBox.Pressed);
		SetUrbanLevel(0);
	}

	private void SetupFarm()
	{
		farmLevelSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/FarmLevelSlider");
		farmLevelSlider.Connect("value_changed", this, "SetFarmLevel");

		farmLevelSlider.MinValue = 0;
		farmLevelSlider.MaxValue = 3;
		farmLevelSlider.Value = 0;
		farmLevelSlider.Step = 1;

		farmLevelCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyFarmLevel");
		farmLevelCheckBox.Connect("toggled", this, "SetApplyFarmLevel");

		SetApplyFarmLevel(farmLevelCheckBox.Pressed);
		SetFarmLevel(0);
	}

	private void SetupPlant()
	{
		plantLevelSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/PlantLevelSlider");
		plantLevelSlider.Connect("value_changed", this, "SetPlantLevel");

		plantLevelSlider.MinValue = 0;
		plantLevelSlider.MaxValue = 3;
		plantLevelSlider.Value = 0;
		plantLevelSlider.Step = 1;

		plantLevelCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyPlantLevel");
		plantLevelCheckBox.Connect("toggled", this, "SetApplyPlantLevel");

		SetApplyPlantLevel(plantLevelCheckBox.Pressed);
		SetPlantLevel(0);
	}

	private void SetupElevation()
	{
		elevationSlider = GetNode<HSlider>("CanvasLayer/Panel/Options/ElevationSlider");
		elevationSlider.Connect("value_changed", this, "SetElevation");

		elevationSlider.MinValue = 0;
		elevationSlider.MaxValue = maxElevation;
		elevationSlider.Value = 0;
		elevationSlider.Step = 1;

		additiveElevationCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/AdditiveElevation");
		additiveElevationCheckBox.Connect("toggled", this, "SetAdditiveElevation");

		elevationCheckBox = GetNode<CheckBox>("CanvasLayer/Panel/Options/ApplyElevation");
		elevationCheckBox.Connect("toggled", this, "SetApplyElevation");

		SetApplyElevation(elevationCheckBox.Pressed);
		SetAdditiveElevation(additiveElevationCheckBox.Pressed);
		SetElevation(0);
	}
}
