using Godot;
using System;

public class HexCell : Spatial
{
	public int TerrainTypeIndex = 0;

	public HexCellShaderData ShaderData { get; set; }

	private HexCell[] neighbors = new HexCell[6];

	private bool[] roads = new bool[6];
	
	private Sprite3D highlight;

	private HexMetrics.Direction incomingRiver, outgoingRiver;

	private int elevation = int.MinValue;

	private Color color = new Color("00000000");

	private bool hasIncomingRiver, hasOutgoingRiver;

	public int Index { get; set; }

	public int SearchPhase { set; get; }
	
	public int MovementCost
	{
		get
		{
			int cost = 1;
			if (IsUnderwater)
			{
				cost = 3;
			}
			return cost;
		}
	}
	public int Distance
	{
		get
		{
			return distance;
		}
		set
		{
			distance = value;
		}
	}

	private int distance;

	public int Elevation
	{
		set
		{
			if (value == elevation)
			{
				return;
			}

			Vector3 position = Translation;
			position.y = value * HexMetrics.ElevationStep;
			position.y += (HexMetrics.SampleNoise(position).g * 2f - 1f) * HexMetrics.ElevationPerturpStrength;
			Translation = position;

			elevation = value;

			ValidateRivers();

			for (int i = 0; i < roads.Length; i++)
			{
				if (roads[i] && GetElevationDifference((HexMetrics.Direction)i) > 1)
				{
					SetRoad(i, false);
				}
			}
		}
		get { return elevation; }
	}

	public int WaterLevel
	{
		get
		{
			return waterLevel;
		}
		set
		{
			if (waterLevel == value)
			{
				return;
			}
			waterLevel = value;
			ValidateRivers();
		}
	}

	int waterLevel = int.MinValue;

	public int UrbanLevel
	{
		get
		{
			return urbanLevel;
		}
		set
		{
			if (urbanLevel != value)
			{
				urbanLevel = value;
			}
		}
	}

	public int FarmLevel
	{
		get
		{
			return farmLevel;
		}
		set
		{
			if (farmLevel != value)
			{
				farmLevel = value;
			}
		}
	}

	public int PlantLevel
	{
		get
		{
			return plantLevel;
		}
		set
		{
			if (plantLevel != value)
			{
				plantLevel = value;
			}
		}
	}

	int urbanLevel, farmLevel, plantLevel;

	public bool Walled
	{
		get
		{
			return walled;
		}
		set
		{
			walled = value;
		}
	}

	private bool walled;

	public HexGridChunk Chunk;

	public Coordinates Coordinates;

	public Color Color
	{
		set
		{
			if (color == value)
			{
				return;
			}

			color = value;
		}
		get
		{
			return color;
		}
	}

	public bool HasIncomingRiver
	{
		get
		{
			return hasIncomingRiver;
		}
	}

	public bool HasOutgoingRiver
	{
		get
		{
			return hasOutgoingRiver;
		}
	}

	public HexMetrics.Direction IncomingRiver
	{
		get
		{
			return incomingRiver;
		}
	}

	public HexMetrics.Direction OutgoingRiver
	{
		get
		{
			return outgoingRiver;
		}
	}

	public HexMetrics.Direction RiverBeginOrEndDirection
	{
		get
		{
			return hasIncomingRiver ? incomingRiver : outgoingRiver;
		}
	}

	public float StreamBedHeight
	{
		get
		{
			return (elevation + HexMetrics.StreamBedElevationOffset) * HexMetrics.ElevationStep;
		}
	}

	public float RiverSurfaceWidth
	{
		get
		{
			return (elevation + HexMetrics.WaterElevationOffset) * HexMetrics.ElevationStep;
		}
	}

	public float WaterSurfaceWidth
	{
		get
		{
			return (waterLevel + HexMetrics.WaterElevationOffset) * HexMetrics.ElevationStep;
		}
	}

	public bool HasRiver
	{
		get
		{
			return hasIncomingRiver || hasOutgoingRiver;
		}
	}

	public bool HasRiverBeginOrEnd
	{
		get
		{
			return hasIncomingRiver != hasOutgoingRiver;
		}
	}

	public bool IsUnderwater
	{
		get
		{
			return waterLevel > elevation;
		}
	}

	public bool IsVisible
	{
		get
		{
			return visibility > 0;
		}
	}

	private int visibility;

	public bool HasRoads
	{
		get
		{
			for (int i = 0; i < roads.Length; i++)
			{
				if (roads[i])
				{
					return true;
				}
			}
			return false;
		}
	}

	public HexCell PathFrom { get; set; }

	public int SearchHeuristic { get; set; }

	public int SearchPriority
	{
		get
		{
			return distance + SearchHeuristic;
		}
	}

	public HexCell NextWithSamePriority { get; set; }


	public static HexCell Instance()
	{
		return GD.Load<PackedScene>("res://src/map/HexCell.tscn").Instance() as HexCell;
	}

	public override void _Ready()
	{
		highlight = GetNode<Sprite3D>("Highlight");
	}

	public void Refresh()
	{
		if (Chunk == null)
		{
			return;
		}

		Chunk.Refresh();
		
		ShaderData.RefreshTerrain(this);
		
		for (int i = 0; i < neighbors.Length; i++)
		{
			if (neighbors[i] != null
				&& neighbors[i].Chunk != null)
			{
				neighbors[i].Chunk.Refresh();
			}
		}
	}

	private void RefreshSelfOnly()
	{
		if (Chunk == null)
		{
			return;
		}

		Chunk.Refresh();
	}

	public HexCell GetNeighbor(HexMetrics.Direction direction)
	{
		return neighbors[(int)direction];
	}

	public HexMetrics.EdgeType GetEdgeType(HexMetrics.Direction direction)
	{
		return HexMetrics.GetEdgeType(elevation, neighbors[(int)direction].elevation);
	}

	public HexMetrics.EdgeType GetEdgeType(HexCell otherCell)
	{
		return HexMetrics.GetEdgeType(elevation, otherCell.elevation);
	}

	public void SetNeighbor(HexMetrics.Direction direction, HexCell cell)
	{
		neighbors[(int)direction] = cell;
		cell.neighbors[(int)direction.Opposite()] = this;
	}

	public void EnableHighlight(Color color)
	{
		highlight.Visible = true;
		var mat = highlight.MaterialOverride;
		mat.Set("albedo_color", color);
	}

	public void DisableHighlight()
	{
		highlight.Visible = false;
	}

	public void IncreaseVisibility()
	{
		visibility = 1;
		if (visibility == 1)
		{
			ShaderData.RefreshVisibility(this);
		}
	}

	public void DecreaseVisibility()
	{
		visibility = 0;
		if (visibility == 0)
		{
			ShaderData.RefreshVisibility(this);
		}
	}

	public void RemoveOutgoingRiver()
	{
		if (!hasOutgoingRiver)
		{
			return;
		}

		hasOutgoingRiver = false;

		HexCell neighbor = GetNeighbor(outgoingRiver);
		neighbor.hasIncomingRiver = false;
	}

	public void RemoveIncomingRiver()
	{
		if (!hasIncomingRiver)
		{
			return;
		}

		hasIncomingRiver = false;

		HexCell neighbor = GetNeighbor(incomingRiver);
		neighbor.hasOutgoingRiver = false;
	}

	public void RemoveRiver()
	{
		RemoveOutgoingRiver();
		RemoveIncomingRiver();
	}

	public void SetOutgoingRiver(HexMetrics.Direction direction)
	{
		if (hasOutgoingRiver && outgoingRiver == direction)
		{
			return;
		}

		HexCell neighbor = GetNeighbor(direction);

		if (!IsValidRiverDestination(neighbor))
		{
			return;
		}

		RemoveOutgoingRiver();

		if (hasIncomingRiver && incomingRiver == direction)
		{
			RemoveIncomingRiver();
		}

		hasOutgoingRiver = true;
		outgoingRiver = direction;

		neighbor.RemoveIncomingRiver();
		neighbor.hasIncomingRiver = true;
		neighbor.incomingRiver = direction.Opposite();

		SetRoad((int)direction, false);

	}

	public bool HasRiverThroughEdge(HexMetrics.Direction direction)
	{
		return hasIncomingRiver && incomingRiver == direction ||
			hasOutgoingRiver && outgoingRiver == direction;
	}

	public void AddRoad(HexMetrics.Direction direction)
	{
		if (!roads[(int)direction]
			&& !HasRiverThroughEdge(direction)
			&& GetElevationDifference(direction) <= 1)
		{
			SetRoad((int)direction, true);
		}
	}

	public void RemoveRoads()
	{
		for (int i = 0; i < neighbors.Length; i++)
		{
			if (roads[i])
			{
				SetRoad(i, false);
			}
		}
	}

	public int GetElevationDifference(HexMetrics.Direction direction)
	{
		int difference = elevation - GetNeighbor(direction).elevation;
		return difference >= 0 ? difference : -difference;
	}


	private void SetRoad(int index, bool state)
	{
		roads[index] = state;
		neighbors[index].roads[(int)((HexMetrics.Direction)index).Opposite()] = state;
	}

	public bool HasRoadThroughEdge(HexMetrics.Direction direction)
	{
		return roads[(int)direction];
	}

	private bool IsValidRiverDestination(HexCell neighbor)
	{
		return neighbor != null && (Elevation >= neighbor.Elevation || WaterLevel == neighbor.Elevation);
	}

	private void ValidateRivers()
	{
		if (
			hasOutgoingRiver && !IsValidRiverDestination(GetNeighbor(outgoingRiver))
		)
		{
			RemoveOutgoingRiver();
		}
		if (
			hasIncomingRiver && !GetNeighbor(incomingRiver).IsValidRiverDestination(this)
		)
		{
			RemoveIncomingRiver();
		}
	}

}