using Godot;
using System;

public class Coordinates
{
	public int X { set; get; }
	public int Z { set; get; }

	public int Y { get { return -X - Z; } }

	public Coordinates()
	{

	}

	public Coordinates(int x, int z)
	{
		X = x;
		Z = z;
	}

	public static Coordinates FromWorld(Vector3 position)
	{
		float x = position.x / (HexMetrics.InnerRadius * 2f);
		float y = -x;

		float offset = position.z / (HexMetrics.OuterRadius * 3f);
		x -= offset;
		y -= offset;

		int iX = Mathf.RoundToInt(x);
		int iY = Mathf.RoundToInt(y);
		int iZ = Mathf.RoundToInt(-x - y);

		if (iX + iY + iZ != 0)
		{
			float dX = Mathf.Abs(x - iX);
			float dY = Mathf.Abs(y - iY);
			float dZ = Mathf.Abs(-x - y - iZ);

			if (dX > dY && dX > dZ)
			{
				iX = -iY - iZ;
			}
			else if (dZ > dY)
			{
				iZ = -iX - iY;
			}
		}

		return new Coordinates(iX, iZ);
	}

	public static Coordinates FromOffsetCoordinates(int x, int z)
	{
		return new Coordinates(x - z / 2, z);
	}

	public int DistanceTo(Coordinates other)
	{
		return ((X < other.X ? other.X - X : X - other.X)
			+ (Y < other.Y ? other.Y - Y : Y - other.Y)
			+ (Z < other.Z ? other.Z - Z : Z - other.Z)) 
			/ 2;
	}

	public int GetIndex(int width)
	{
		return X + Z * width + Z / 2;
	}

	public override string ToString()
	{
		return "" + X + "\n" + Y + "\n" + Z;
	}
}
