using Godot;
using System;

public class HexGridChunk : Spatial
{

	static Color Weight1 = new Color(1f, 0f, 0f);
	static Color Weight2 = new Color(0f, 1f, 0f);
	static Color Weight3 = new Color(0f, 0f, 1f);


	private HexCell[] cells;

	public HexMesh Terrain;
	public HexMesh Rivers;
	public HexMesh Roads;
	public HexMesh Water;
	public HexMesh WaterShore;
	public HexMesh Estuaries;

	public HexFeatureManager Features;

	private bool enabled = false;

	public static HexGridChunk Instance()
	{
		return GD.Load<PackedScene>("res://src/map/HexGridChunk.tscn").Instance() as HexGridChunk;
	}

	public override void _Ready()
	{
		Terrain = GetNode<HexMesh>("Terrain");
		Rivers = GetNode<HexMesh>("Rivers");
		Roads = GetNode<HexMesh>("Roads");
		Water = GetNode<HexMesh>("Water");
		WaterShore = GetNode<HexMesh>("WaterShore");
		Estuaries = GetNode<HexMesh>("Estuaries");

		Features = GetNode<HexFeatureManager>("Features");

		cells = new HexCell[HexMetrics.ChunkWidth * HexMetrics.ChunkHeight];

		SetupTerrrainMaterial();
	}

	private void SetupTerrrainMaterial()
	{
		Data data = GetNode<Data>("/root/Data");
		var material = Terrain.MaterialOverride;

		var texArray = data.GetTextureArray();
		material.Set("shader_param/textures", texArray);
		Terrain.MaterialOverride = material;
	}

	public void Refresh()
	{
		enabled = true;
		CallDeferred("Triangulate");
	}

	public void Triangulate()
	{
		if (enabled)
		{
			Triangulate(cells);
		}

		enabled = false;
	}

	public void AddCell(int index, HexCell cell)
	{
		cells[index] = cell;
		cell.Chunk = this;
		AddChild(cell);
	}

	public void Triangulate(HexCell[] cells)
	{

		Terrain.Clear();
		Rivers.Clear();
		Roads.Clear();
		Water.Clear();
		WaterShore.Clear();
		Estuaries.Clear();

		Features.Clear();

		for (int i = 0; i < cells.Length; i++)
		{
			Triangulate(cells[i]);
		}

		Terrain.Apply();
		Rivers.Apply();
		Roads.Apply();
		Water.Apply();
		WaterShore.Apply();
		Estuaries.Apply();

		Features.Apply();
	}

	private void Triangulate(HexCell cell)
	{
		if (cell == null)
		{
			return;
		}

		Vector3 center = cell.Translation;

		for (HexMetrics.Direction d = HexMetrics.Direction.NE; d <= HexMetrics.Direction.NW; d++)
		{
			Triangulate(d, cell);
		}

		if (!cell.IsUnderwater && !cell.HasRiver && !cell.HasRoads)
		{
			Features.AddFeature(cell, cell.Translation);
		}
	}

	private void Triangulate(HexMetrics.Direction direction, HexCell cell)
	{
		Vector3 center = cell.Translation;

		EdgeVertices e = new EdgeVertices(
			center + HexMetrics.GetFirstSolidCorner(direction),
			center + HexMetrics.GetSecondSolidCorner(direction)
		);

		if (cell.HasRiver)
		{
			if (cell.HasRiverThroughEdge(direction))
			{
				e.v3.y = cell.StreamBedHeight;
				if (cell.HasRiverBeginOrEnd)
				{
					TriangulateWithRiverBeginOrEnd(direction, cell, center, e);
				}
				else
				{
					TriangulateWithRiver(direction, cell, center, e);
				}
			}
			else
			{
				TriangulateAdjacentToRiver(direction, cell, center, e);
			}
		}
		else
		{
			TriangulateWithoutRiver(direction, cell, center, e);

			if (!cell.IsUnderwater && !cell.HasRoadThroughEdge(direction))
			{
				Features.AddFeature(cell, (center + e.v1 + e.v5) * (1f / 3f));
			}
		}

		if (direction <= HexMetrics.Direction.SE)
		{
			TriangulateConnection(direction, cell, e);
		}

		if (cell.IsUnderwater)
		{
			TriangulateWater(direction, cell, center);
		}
	}

	private void TriangulateWater(HexMetrics.Direction direction, HexCell cell, Vector3 center)
	{
		center.y = cell.WaterSurfaceWidth;

		HexCell neighbor = cell.GetNeighbor(direction);

		if (neighbor != null && !neighbor.IsUnderwater)
		{
			TriangulateWaterShore(direction, cell, neighbor, center);
		}
		else
		{
			TriangulateOpenWater(direction, cell, neighbor, center);
		}


	}

	private void TriangulateOpenWater(HexMetrics.Direction direction, HexCell cell, HexCell neighbor, Vector3 center)
	{
		Vector3 c1 = center + HexMetrics.GetFirstWaterCorner(direction);
		Vector3 c2 = center + HexMetrics.GetSecondWaterCorner(direction);

		Water.AddTriangle(center, c1, c2);
		Vector3 indices;
		indices.x = indices.y = indices.z = cell.Index;
		Water.AddTriangleCellData(indices, Weight1);

		if (direction <= HexMetrics.Direction.SE && neighbor != null)
		{
			Vector3 bridge = HexMetrics.GetWaterBridge(direction);
			Vector3 e1 = c1 + bridge;
			Vector3 e2 = c2 + bridge;

			Water.AddQuad(c1, c2, e1, e2);
			indices.y = neighbor.Index;
			Water.AddQuadCellData(indices, Weight1, Weight2);

			if (direction <= HexMetrics.Direction.E)
			{
				HexCell nextNeighbor = cell.GetNeighbor(direction.Next());

				if (nextNeighbor == null || !nextNeighbor.IsUnderwater)
				{
					return;
				}

				Water.AddTriangle(c2, e2, c2 + HexMetrics.GetWaterBridge(direction.Next()));
				indices.z = nextNeighbor.Index;
				Water.AddTriangleCellData(indices, Weight1, Weight2, Weight3);
			}
		}
	}

	private void TriangulateWaterShore(HexMetrics.Direction direction, HexCell cell, HexCell neighbor, Vector3 center)
	{
		EdgeVertices e1 = new EdgeVertices(
			center + HexMetrics.GetFirstWaterCorner(direction),
			center + HexMetrics.GetSecondWaterCorner(direction)
		);

		Water.AddTriangle(center, e1.v1, e1.v2);
		Water.AddTriangle(center, e1.v2, e1.v3);
		Water.AddTriangle(center, e1.v3, e1.v4);
		Water.AddTriangle(center, e1.v4, e1.v5);
		Vector3 indices;

		indices.x = indices.z = cell.Index;
		indices.y = neighbor.Index;

		Water.AddTriangleCellData(indices, Weight1);
		Water.AddTriangleCellData(indices, Weight1);
		Water.AddTriangleCellData(indices, Weight1);
		Water.AddTriangleCellData(indices, Weight1);

		Vector3 center2 = neighbor.Translation;
		center2.y = center.y;

		EdgeVertices e2 = new EdgeVertices(
			center2 + HexMetrics.GetSecondSolidCorner(direction.Opposite()),
			center2 + HexMetrics.GetFirstSolidCorner(direction.Opposite())
		);

		if (cell.HasRiverThroughEdge(direction))
		{
			TriangulateEstuary(e1, e2, cell.IncomingRiver == direction, indices);
		}
		else
		{
			WaterShore.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
			WaterShore.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
			WaterShore.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
			WaterShore.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);

			WaterShore.AddQuadUV(0f, 0f, 0f, 1f);
			WaterShore.AddQuadUV(0f, 0f, 0f, 1f);
			WaterShore.AddQuadUV(0f, 0f, 0f, 1f);
			WaterShore.AddQuadUV(0f, 0f, 0f, 1f);

			WaterShore.AddQuadCellData(indices, Weight1, Weight2);
			WaterShore.AddQuadCellData(indices, Weight1, Weight2);
			WaterShore.AddQuadCellData(indices, Weight1, Weight2);
			WaterShore.AddQuadCellData(indices, Weight1, Weight2);
		}

		HexCell nextNeighbor = cell.GetNeighbor(direction.Next());

		if (nextNeighbor != null)
		{
			Vector3 v3 = nextNeighbor.Translation + (nextNeighbor.IsUnderwater
				? HexMetrics.GetFirstWaterCorner(direction.Previous())
				: HexMetrics.GetFirstSolidCorner(direction.Previous())
			);

			v3.y = center.y;

			WaterShore.AddTriangle(e1.v5, e2.v5, v3);
			WaterShore.AddTriangleUV(
				new Vector2(0f, 0f),
				new Vector2(0f, 1f),
				new Vector2(0f, nextNeighbor.IsUnderwater ? 0f : 1f)
			);
			indices.z = nextNeighbor.Index;
			WaterShore.AddTriangleCellData(
				indices, Weight1, Weight2, Weight3
			);
		}
	}

	private void TriangulateEstuary(EdgeVertices e1, EdgeVertices e2, bool incomingRiver, Vector3 indices)
	{
		WaterShore.AddTriangle(e2.v1, e1.v2, e1.v1);
		WaterShore.AddTriangle(e2.v5, e1.v5, e1.v4);

		WaterShore.AddTriangleUV(new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f));
		WaterShore.AddTriangleUV(new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f));

		WaterShore.AddTriangleCellData(indices, Weight2, Weight1, Weight1);
		WaterShore.AddTriangleCellData(indices, Weight2, Weight1, Weight1);

		Estuaries.AddQuad(e2.v1, e1.v2, e2.v2, e1.v3);
		Estuaries.AddTriangle(e1.v3, e2.v2, e2.v4);
		Estuaries.AddQuad(e1.v3, e1.v4, e2.v4, e2.v5);

		Estuaries.AddQuadUV(
			new Vector2(0f, 1f), new Vector2(0f, 0f),
			new Vector2(0f, 1f), new Vector2(0f, 0f)
		);

		Estuaries.AddTriangleUV(
			new Vector2(0f, 0f),
			new Vector2(0f, 1f),
			new Vector2(0f, 1f)
		);

		Estuaries.AddQuadUV(0f, 0f, 0f, 1f);

		Estuaries.AddQuadCellData(
			indices, Weight2, Weight1, Weight2, Weight1
		);
		Estuaries.AddTriangleCellData(indices, Weight1, Weight2, Weight2);
		Estuaries.AddQuadCellData(indices, Weight1, Weight2);

		if (incomingRiver)
		{
			Estuaries.AddQuadUV2(
				new Vector2(1.5f, 1f), new Vector2(0.7f, 1.15f),
				new Vector2(1f, 0.8f), new Vector2(0.5f, 1.1f)
			);
			Estuaries.AddTriangleUV2(
				new Vector2(0.5f, 1.1f),
				new Vector2(1f, 0.8f),
				new Vector2(0f, 0.8f)
			);
			Estuaries.AddQuadUV2(
				new Vector2(0.5f, 1.1f), new Vector2(0.3f, 1.15f),
				new Vector2(0f, 0.8f), new Vector2(-0.5f, 1f)
			);
		}
		else
		{
			Estuaries.AddQuadUV2(
				new Vector2(-0.5f, -0.2f), new Vector2(0.3f, -0.35f),
				new Vector2(0f, 0f), new Vector2(0.5f, -0.3f)
			);
			Estuaries.AddTriangleUV2(
				new Vector2(0.5f, -0.3f),
				new Vector2(0f, 0f),
				new Vector2(1f, 0f)
			);
			Estuaries.AddQuadUV2(
				new Vector2(0.5f, -0.3f), new Vector2(0.7f, -0.35f),
				new Vector2(1f, 0f), new Vector2(1.5f, -0.2f)
			);
		}
	}

	private void TriangulateWaterfallInWater(
		Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4,
		float y1, float y2, float waterY)
	{
		v1.y = v2.y = y1;
		v3.y = v4.y = y2;

		v1 = HexMetrics.Perturb(v1);
		v2 = HexMetrics.Perturb(v2);
		v3 = HexMetrics.Perturb(v3);
		v4 = HexMetrics.Perturb(v4);

		float t = (waterY - y2) / (y1 - y2);
		v3 = v3.LinearInterpolate(v1, t);
		v4 = v4.LinearInterpolate(v2, t);

		Rivers.AddQuadUnperturbed(v1, v2, v3, v4);
		Rivers.AddQuadUV(0f, 1f, 0.8f, 1f);
	}

	private void TriangulateAdjacentToRiver(HexMetrics.Direction direction, HexCell cell, Vector3 center, EdgeVertices e)
	{
		if (cell.HasRoads)
		{
			TriangulateRoadAdjacentToRiver(direction, cell, center, e);
		}

		if (cell.HasRiverThroughEdge(direction.Next()))
		{
			if (cell.HasRiverThroughEdge(direction.Previous()))
			{
				center += HexMetrics.GetSolidEdgeMiddle(direction) * (HexMetrics.InnerToOuter * 0.5f);
			}
			else if (cell.HasRiverThroughEdge(direction.Previous2()))
			{
				center += HexMetrics.GetFirstSolidCorner(direction) * 0.25f;
			}
		}
		else if (cell.HasRiverThroughEdge(direction.Previous()) && cell.HasRiverThroughEdge(direction.Next2()))
		{
			center += HexMetrics.GetSecondSolidCorner(direction) * 0.25f;
		}


		EdgeVertices m = new EdgeVertices(
			center.LinearInterpolate(e.v1, 0.5f),
			center.LinearInterpolate(e.v5, 0.5f)
		);

		TriangulateEdgeStrip(m, Weight1, cell.Index, e, Weight1, cell.Index);
		TriangulateEdgeFan(center, m, cell.Index);

		if (!cell.IsUnderwater && !cell.HasRoadThroughEdge(direction))
		{
			Features.AddFeature(cell, (center + e.v1 + e.v5) * (1f / 3f));
		}
	}

	private void TriangulateRoadAdjacentToRiver(HexMetrics.Direction direction, HexCell cell, Vector3 center, EdgeVertices e)
	{
		bool hasRoadThroughEdge = cell.HasRoadThroughEdge(direction);
		bool previousHasRiver = cell.HasRiverThroughEdge(direction.Previous());
		bool nextHasRiver = cell.HasRiverThroughEdge(direction.Next());

		Vector3 roadCenter = center;
		Vector2 interpolators = GetRoadInterpolators(direction, cell);

		if (cell.HasRiverBeginOrEnd)
		{
			roadCenter += HexMetrics.GetSolidEdgeMiddle(cell.RiverBeginOrEndDirection.Opposite()) * (1f / 3f);
		}
		else if (cell.IncomingRiver == cell.OutgoingRiver.Opposite())
		{
			Vector3 corner;

			if (previousHasRiver)
			{
				if (!hasRoadThroughEdge && !cell.HasRoadThroughEdge(direction.Next()))
				{
					return;
				}

				corner = HexMetrics.GetSecondSolidCorner(direction);
			}
			else
			{
				if (!hasRoadThroughEdge && !cell.HasRoadThroughEdge(direction.Previous()))
				{
					return;
				}

				corner = HexMetrics.GetFirstSolidCorner(direction);
			}

			roadCenter += corner * 0.5f;
			center += corner * 0.25f;
		}
		else if (cell.IncomingRiver == cell.OutgoingRiver.Previous())
		{
			roadCenter -= HexMetrics.GetSecondCorner(cell.IncomingRiver) * 0.2f;
		}
		else if (cell.IncomingRiver == cell.OutgoingRiver.Next())
		{
			roadCenter -= HexMetrics.GetFirstCorner(cell.IncomingRiver) * 0.2f;
		}
		else if (previousHasRiver && nextHasRiver)
		{
			if (!hasRoadThroughEdge)
			{
				return;
			}
			Vector3 offset = HexMetrics.GetSolidEdgeMiddle(direction) * HexMetrics.InnerToOuter;
			roadCenter += offset * 0.7f;
			center += offset * 0.5f;
		}
		else
		{
			HexMetrics.Direction middle;
			if (previousHasRiver)
			{
				middle = direction.Next();
			}
			else if (nextHasRiver)
			{
				middle = direction.Previous();
			}
			else
			{
				middle = direction;
			}

			if (!cell.HasRoadThroughEdge(middle)
				&& !cell.HasRoadThroughEdge(middle.Previous())
				&& !cell.HasRoadThroughEdge(middle.Next()))
			{
				return;
			}

			roadCenter += HexMetrics.GetSolidEdgeMiddle(middle) * 0.25f;
		}

		Vector3 mL = roadCenter.LinearInterpolate(e.v1, interpolators.x);
		Vector3 mR = roadCenter.LinearInterpolate(e.v5, interpolators.y);

		TriangulateRoad(roadCenter, mL, mR, e, hasRoadThroughEdge, cell.Index);

		if (previousHasRiver)
		{
			TriangulateRoadEdge(roadCenter, center, mL, cell.Index);
		}

		if (nextHasRiver)
		{
			TriangulateRoadEdge(roadCenter, mR, center, cell.Index);
		}
	}

	private void TriangulateWithRiverBeginOrEnd(HexMetrics.Direction direction, HexCell cell, Vector3 center, EdgeVertices e)
	{
		EdgeVertices m = new EdgeVertices(
			center.LinearInterpolate(e.v1, 0.5f),
			center.LinearInterpolate(e.v5, 0.5f)
		);

		m.v3.y = e.v3.y;

		TriangulateEdgeStrip(m, Weight1, cell.Index, e, Weight1, cell.Index);
		TriangulateEdgeFan(center, m, cell.Index);

		if (!cell.IsUnderwater)
		{
			bool reversed = cell.HasIncomingRiver;

			TriangulateRiverQuad(m.v2, m.v4, e.v2, e.v4, cell.RiverSurfaceWidth, 0.6f, reversed);

			center.y = m.v2.y = m.v4.y = cell.RiverSurfaceWidth;

			Rivers.AddTriangle(center, m.v2, m.v4);

			if (reversed)
			{
				Rivers.AddTriangleUV(new Vector2(0.5f, 0.4f), new Vector2(1f, 0.2f), new Vector2(0f, 0.2f));
			}
			else
			{
				Rivers.AddTriangleUV(new Vector2(0.5f, 0.4f), new Vector2(0f, 0.6f), new Vector2(1f, 0.6f));
			}
		}
	}

	private void TriangulateWithRiver(HexMetrics.Direction direction, HexCell cell, Vector3 center, EdgeVertices e)
	{
		Vector3 centerL, centerR;

		if (cell.HasRiverThroughEdge(direction.Opposite()))
		{
			centerL = center + HexMetrics.GetFirstSolidCorner(direction.Previous()) * 0.25f;
			centerR = center + HexMetrics.GetSecondSolidCorner(direction.Next()) * 0.25f;
		}
		else if (cell.HasRiverThroughEdge(direction.Next()))
		{
			centerL = center;
			centerR = center.LinearInterpolate(e.v5, 2f / 3f);
		}
		else if (cell.HasRiverThroughEdge(direction.Previous()))
		{
			centerL = center.LinearInterpolate(e.v1, 2f / 3f);
			centerR = center;
		}
		else if (cell.HasRiverThroughEdge(direction.Next2()))
		{
			centerL = center;
			centerR = center + HexMetrics.GetSolidEdgeMiddle(direction.Next()) * 0.5f * HexMetrics.InnerToOuter;
		}
		else
		{
			centerL = center + HexMetrics.GetSolidEdgeMiddle(direction.Previous()) * 0.5f * HexMetrics.InnerToOuter;
			centerR = center;
		}

		center = centerL.LinearInterpolate(centerR, 0.5f);


		EdgeVertices m = new EdgeVertices(
			centerL.LinearInterpolate(e.v1, 0.5f),
			centerR.LinearInterpolate(e.v5, 0.5f),
			1f / 6f
		);

		m.v3.y = center.y = e.v3.y;

		TriangulateEdgeStrip(m, Weight1, cell.Index, e, Weight1, cell.Index);

		Terrain.AddTriangle(centerL, m.v1, m.v2);
		Terrain.AddQuad(centerL, center, m.v2, m.v3);
		Terrain.AddQuad(center, centerR, m.v3, m.v4);
		Terrain.AddTriangle(centerR, m.v4, m.v5);

		Vector3 indices;
		indices.x = indices.y = indices.z = cell.Index;
		Terrain.AddTriangleCellData(indices, Weight1);
		Terrain.AddQuadCellData(indices, Weight1);
		Terrain.AddQuadCellData(indices, Weight1);
		Terrain.AddTriangleCellData(indices, Weight1);

		if (!cell.IsUnderwater)
		{
			bool reversed = cell.IncomingRiver == direction;

			TriangulateRiverQuad(centerL, centerR, m.v2, m.v4, cell.RiverSurfaceWidth, 0.4f, reversed);
			TriangulateRiverQuad(m.v2, m.v4, e.v2, e.v4, cell.RiverSurfaceWidth, 0.6f, reversed);
		}
	}

	private void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y, float v, bool reversed)
	{
		TriangulateRiverQuad(v1, v2, v3, v4, y, y, v, reversed);
	}

	private void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, float v, bool reversed)
	{
		v1.y = v2.y = y1;
		v3.y = v4.y = y2;

		Rivers.AddQuad(v1, v2, v3, v4);

		if (reversed)
		{
			Rivers.AddQuadUV(1f, 0f, 0.8f - v, 0.6f - v);
		}
		else
		{
			Rivers.AddQuadUV(0f, 1f, v, v + 0.2f);
		}
	}

	private void TriangulateRoad(
		Vector3 center, Vector3 mL, Vector3 mR,
		EdgeVertices e, bool hasRoadThroughCellEdge, float index)
	{
		if (hasRoadThroughCellEdge)
		{
			Vector3 indices;
			indices.x = indices.y = indices.z = index;

			Vector3 mC = mL.LinearInterpolate(mR, 0.5f);

			TriangulateRoadSegment(mL, mC, mR, e.v2, e.v3, e.v4, Weight1, Weight1, indices);

			Roads.AddTriangle(center, mL, mC);
			Roads.AddTriangle(center, mC, mR);

			Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(1f, 0f));
			Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(0f, 0f));

			Roads.AddTriangleCellData(indices, Weight1);
			Roads.AddTriangleCellData(indices, Weight1);
		}
		else
		{
			TriangulateRoadEdge(center, mL, mR, index);
		}
	}

	private void TriangulateRoadSegment(
		Vector3 v1, Vector3 v2, Vector3 v3,
		Vector3 v4, Vector3 v5, Vector3 v6,
		Color w1, Color w2, Vector3 indices)
	{
		Roads.AddQuad(v1, v2, v4, v5);
		Roads.AddQuad(v2, v3, v5, v6);

		Roads.AddQuadUV(0f, 1f, 0f, 0f);
		Roads.AddQuadUV(1f, 0f, 0f, 0f);

		Roads.AddQuadCellData(indices, w1, w2);
		Roads.AddQuadCellData(indices, w1, w2);
	}

	private void TriangulateRoadEdge(Vector3 center, Vector3 mL, Vector3 mR, float index)
	{
		Roads.AddTriangle(center, mL, mR);
		Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
		Vector3 indices;
		indices.x = indices.y = indices.z = index;
		Roads.AddTriangleCellData(indices, Weight1);
	}

	private void TriangulateWithoutRiver(
		HexMetrics.Direction direction, HexCell cell, Vector3 center, EdgeVertices e)
	{
		TriangulateEdgeFan(center, e, cell.Index);

		if (cell.HasRoads)
		{
			Vector2 interpolators = GetRoadInterpolators(direction, cell);

			TriangulateRoad(
				center,
				center.LinearInterpolate(e.v1, interpolators.x),
				center.LinearInterpolate(e.v5, interpolators.y),
				e, cell.HasRoadThroughEdge(direction), cell.Index
			);
		}
	}

	private Vector2 GetRoadInterpolators(HexMetrics.Direction direction, HexCell cell)
	{
		Vector2 interpolators;
		if (cell.HasRoadThroughEdge(direction))
		{
			interpolators.x = interpolators.y = 0.5f;
		}
		else
		{
			interpolators.x = cell.HasRoadThroughEdge(direction.Previous()) ? 0.5f : 0.25f;
			interpolators.y = cell.HasRoadThroughEdge(direction.Next()) ? 0.5f : 0.25f;
		}
		return interpolators;
	}

	private void TriangulateConnection(HexMetrics.Direction direction, HexCell cell, EdgeVertices e1)
	{
		HexCell neighbor = cell.GetNeighbor(direction);

		if (neighbor == null)
		{
			return;
		}

		Vector3 bridge = HexMetrics.GetBridge(direction);
		bridge.y = neighbor.Translation.y - cell.Translation.y;
		EdgeVertices e2 = new EdgeVertices(
			e1.v1 + bridge,
			e1.v5 + bridge
		);

		if (cell.HasRiverThroughEdge(direction))
		{
			e2.v3.y = neighbor.StreamBedHeight;

			if (!cell.IsUnderwater)
			{
				if (!neighbor.IsUnderwater)
				{
					TriangulateRiverQuad(
						e1.v2, e1.v4, e2.v2, e2.v4,
						cell.RiverSurfaceWidth, neighbor.RiverSurfaceWidth, 0.8f,
						cell.HasIncomingRiver && cell.IncomingRiver == direction
					);
				}
				else if (cell.Elevation > neighbor.WaterLevel)
				{
					TriangulateWaterfallInWater(
						e1.v2, e1.v4, e2.v2, e2.v4,
						cell.RiverSurfaceWidth, neighbor.RiverSurfaceWidth,
						neighbor.WaterSurfaceWidth
					);
				}
			}
			else if (!neighbor.IsUnderwater && neighbor.Elevation > cell.WaterLevel)
			{
				TriangulateWaterfallInWater(
					e2.v4, e2.v2, e1.v4, e1.v2,
					neighbor.RiverSurfaceWidth, cell.RiverSurfaceWidth,
					cell.WaterSurfaceWidth
				);
			}
		}

		bool hasRiver = cell.HasRiverThroughEdge(direction);
		bool hasRoad = cell.HasRoadThroughEdge(direction);

		if (cell.GetEdgeType(direction) == HexMetrics.EdgeType.Slope)
		{
			TriangulateEdgeTerraces(e1, cell, e2, neighbor, hasRoad);
		}
		else
		{
			TriangulateEdgeStrip(
				e1, Weight1, cell.Index,
				e2, Weight2, neighbor.Index,
				hasRoad);
		}

		Features.AddWall(e1, cell, e2, neighbor, hasRiver, hasRoad);


		HexCell nextNeighbor = cell.GetNeighbor(direction.Next());

		if (direction <= HexMetrics.Direction.E && nextNeighbor != null)
		{
			Vector3 v5 = e1.v5 + HexMetrics.GetBridge(direction.Next());
			v5.y = nextNeighbor.Translation.y;

			if (cell.Elevation <= neighbor.Elevation)
			{
				if (cell.Elevation <= nextNeighbor.Elevation)
				{
					TriangulateCorner(e1.v5, cell, e2.v5, neighbor, v5, nextNeighbor);
				}
				else
				{
					TriangulateCorner(v5, nextNeighbor, e1.v5, cell, e2.v5, neighbor);
				}
			}
			else if (neighbor.Elevation <= nextNeighbor.Elevation)
			{
				TriangulateCorner(e2.v5, neighbor, v5, nextNeighbor, e1.v5, cell);
			}
			else
			{
				TriangulateCorner(v5, nextNeighbor, e1.v5, cell, e2.v5, neighbor);
			}
		}
	}

	private void TriangulateCorner(
		Vector3 bottom, HexCell bottomCell,
		Vector3 left, HexCell leftCell,
		Vector3 right, HexCell rightCell)
	{
		HexMetrics.EdgeType leftEdgeType = bottomCell.GetEdgeType(leftCell);
		HexMetrics.EdgeType rightEdgeType = bottomCell.GetEdgeType(rightCell);

		if (leftEdgeType == HexMetrics.EdgeType.Slope)
		{
			if (rightEdgeType == HexMetrics.EdgeType.Slope)
			{
				TriangulateCornerTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
			}

			else if (rightEdgeType == HexMetrics.EdgeType.Flat)
			{
				TriangulateCornerTerraces(left, leftCell, right, rightCell, bottom, bottomCell);
			}
			else
			{
				TriangulateCornerTerracesCliff(bottom, bottomCell, left, leftCell, right, rightCell);
			}
		}

		else if (rightEdgeType == HexMetrics.EdgeType.Slope)
		{
			if (leftEdgeType == HexMetrics.EdgeType.Flat)
			{
				TriangulateCornerTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
			}
			else
			{
				TriangulateCornerCliffTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
			}

		}

		else if (leftCell.GetEdgeType(rightCell) == HexMetrics.EdgeType.Slope)
		{
			if (leftCell.Elevation < rightCell.Elevation)
			{
				TriangulateCornerCliffTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
			}
			else
			{
				TriangulateCornerTerracesCliff(left, leftCell, right, rightCell, bottom, bottomCell);
			}
		}
		else
		{
			Terrain.AddTriangle(bottom, left, right);
			Vector3 indices;
			indices.x = bottomCell.Index;
			indices.y = leftCell.Index;
			indices.z = rightCell.Index;
			Terrain.AddTriangleCellData(indices, Weight1, Weight2, Weight3);
		}

		Features.AddWall(bottom, bottomCell, left, leftCell, right, rightCell);

	}

	private void TriangulateCornerTerracesCliff(
		Vector3 begin, HexCell beginCell,
		Vector3 left, HexCell leftCell,
		Vector3 right, HexCell rightCell)
	{
		float b = 1f / (rightCell.Elevation - beginCell.Elevation);

		if (b < 0)
		{
			b = -b;
		}

		Vector3 boundary = HexMetrics.Perturb(begin).LinearInterpolate(HexMetrics.Perturb(right), b);
		Color boundaryWeight = Weight1.LinearInterpolate(Weight3, b);

		Vector3 indices;
		indices.x = beginCell.Index;
		indices.y = leftCell.Index;
		indices.z = rightCell.Index;

		TriangulateBoundaryTriangle(begin, Weight1, left, Weight2, boundary, boundaryWeight, indices);

		if (leftCell.GetEdgeType(rightCell) == HexMetrics.EdgeType.Slope)
		{
			TriangulateBoundaryTriangle(left, Weight2, right, Weight3, boundary, boundaryWeight, indices);
		}
		else
		{
			Terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
			Terrain.AddTriangleCellData(indices, Weight2, Weight3, boundaryWeight);
		}
	}

	private void TriangulateCornerCliffTerraces(
		Vector3 begin, HexCell beginCell,
		Vector3 left, HexCell leftCell,
		Vector3 right, HexCell rightCell)
	{
		float b = 1f / (leftCell.Elevation - beginCell.Elevation);

		if (b < 0)
		{
			b = -b;
		}

		Vector3 boundary = HexMetrics.Perturb(begin).LinearInterpolate(HexMetrics.Perturb(left), b);

		Color boundaryWeight = Weight1.LinearInterpolate(Weight2, b);

		Vector3 indices;
		indices.x = beginCell.Index;
		indices.y = leftCell.Index;
		indices.z = rightCell.Index;

		TriangulateBoundaryTriangle(right, Weight3, begin, Weight1, boundary, boundaryWeight, indices);

		if (leftCell.GetEdgeType(rightCell) == HexMetrics.EdgeType.Slope)
		{
			TriangulateBoundaryTriangle(left, Weight2, right, Weight3, boundary, boundaryWeight, indices);
		}
		else
		{
			Terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
			Terrain.AddTriangleCellData(indices, Weight2, Weight3, boundaryWeight);
		}
	}

	private void TriangulateBoundaryTriangle(
		Vector3 begin, Color beginWeight,
		Vector3 left, Color leftWeight,
		Vector3 boundary, Color boundaryWeight, Vector3 indices)
	{
		Vector3 v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, 1));
		Color w2 = HexMetrics.TerraceLerp(beginWeight, leftWeight, 1);

		Terrain.AddTriangleUnperturbed(HexMetrics.Perturb(begin), v2, boundary);
		Terrain.AddTriangleCellData(indices, beginWeight, w2, boundaryWeight);

		for (int i = 2; i < HexMetrics.TerraceSteps; i++)
		{
			Vector3 v1 = v2;
			Color w1 = w2;
			v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, i));
			w2 = HexMetrics.TerraceLerp(beginWeight, leftWeight, i);
			Terrain.AddTriangleUnperturbed(v1, v2, boundary);
			Terrain.AddTriangleCellData(indices, w1, w2, boundaryWeight);
		}

		Terrain.AddTriangleUnperturbed(v2, HexMetrics.Perturb(left), boundary);
		Terrain.AddTriangleCellData(indices, w2, leftWeight, boundaryWeight);
	}

	private void TriangulateCornerTerraces(
		Vector3 begin, HexCell beginCell,
		Vector3 left, HexCell leftCell,
		Vector3 right, HexCell rightCell)
	{
		Vector3 v3 = HexMetrics.TerraceLerp(begin, left, 1);
		Vector3 v4 = HexMetrics.TerraceLerp(begin, right, 1);

		Color w3 = HexMetrics.TerraceLerp(Weight1, Weight2, 1);
		Color w4 = HexMetrics.TerraceLerp(Weight1, Weight3, 1);

		Vector3 indices;
		indices.x = beginCell.Index;
		indices.y = leftCell.Index;
		indices.z = rightCell.Index;

		Terrain.AddTriangle(begin, v3, v4);
		Terrain.AddTriangleCellData(indices, Weight1, w3, w4);

		for (int i = 2; i < HexMetrics.TerraceSteps; i++)
		{
			Vector3 v1 = v3;
			Vector3 v2 = v4;
			Color w1 = w3;
			Color w2 = w4;
			v3 = HexMetrics.TerraceLerp(begin, left, i);
			v4 = HexMetrics.TerraceLerp(begin, right, i);
			w3 = HexMetrics.TerraceLerp(Weight1, Weight2, i);
			w4 = HexMetrics.TerraceLerp(Weight1, Weight3, i);
			Terrain.AddQuad(v1, v2, v3, v4);
			Terrain.AddQuadCellData(indices, w1, w2, w3, w4);
		}

		Terrain.AddQuad(v3, v4, left, right);
		Terrain.AddQuadCellData(indices, w3, w4, Weight2, Weight3);
	}

	private void TriangulateEdgeTerraces(
		EdgeVertices begin, HexCell beginCell,
		EdgeVertices end, HexCell endCell,
		bool hasRoad = false)
	{

		EdgeVertices e2 = EdgeVertices.TerraceLerp(begin, end, 1);
		Color w2 = HexMetrics.TerraceLerp(Weight1, Weight2, 1);

		float i1 = beginCell.Index;
		float i2 = endCell.Index;

		TriangulateEdgeStrip(begin, Weight1, i1, e2, w2, i2, hasRoad);

		for (int i = 2; i < HexMetrics.TerraceSteps; i++)
		{
			EdgeVertices e1 = e2;
			Color w1 = w2;
			e2 = EdgeVertices.TerraceLerp(begin, end, i);
			w2 = HexMetrics.TerraceLerp(Weight1, Weight2, i);
			TriangulateEdgeStrip(e1, w1, i1, e2, w2, i2, hasRoad);
		}

		TriangulateEdgeStrip(e2, w2, i1, end, Weight2, i2, hasRoad);
	}

	private void TriangulateEdgeFan(Vector3 center, EdgeVertices edge, float index)
	{
		Terrain.AddTriangle(center, edge.v1, edge.v2);
		Terrain.AddTriangle(center, edge.v2, edge.v3);
		Terrain.AddTriangle(center, edge.v3, edge.v4);
		Terrain.AddTriangle(center, edge.v4, edge.v5);

		Vector3 indices;
		indices.x = indices.y = indices.z = index;
		Terrain.AddTriangleCellData(indices, Weight1);
		Terrain.AddTriangleCellData(indices, Weight1);
		Terrain.AddTriangleCellData(indices, Weight1);
		Terrain.AddTriangleCellData(indices, Weight1);
	}

	private void TriangulateEdgeStrip(
		EdgeVertices e1, Color w1, float index1,
		EdgeVertices e2, Color w2, float index2,
		bool hasRoad = false)
	{
		Terrain.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
		Terrain.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
		Terrain.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
		Terrain.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);

		Vector3 indices;
		indices.x = indices.z = index1;
		indices.y = index2;
		Terrain.AddQuadCellData(indices, w1, w2);
		Terrain.AddQuadCellData(indices, w1, w2);
		Terrain.AddQuadCellData(indices, w1, w2);
		Terrain.AddQuadCellData(indices, w1, w2);

		if (hasRoad)
		{
			TriangulateRoadSegment(e1.v2, e1.v3, e1.v4, e2.v2, e2.v3, e2.v4, w1, w2, indices);
		}
	}
}
