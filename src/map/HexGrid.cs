using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;

public class HexGrid : Spatial
{

	private HexCell[] cells;
	private HexGridChunk[] chunks;

	private HexCellPriorityQueue searchFrontier;
	private int searchFrontierPhase;

	private HexCell currentPathFrom, currentPathTo;
	private bool currentPathExists;

	private HexCellShaderData cellShaderData = new HexCellShaderData();

	[Export] public int Width = 40;
	[Export] public int Height = 40;

	[Export] private Color defaultColor = new Color("FFFFFF");

	[Export] private int chunkCountWidth = 5;
	[Export] private int chunkCountHeight = 5;

	[Export] public int seed = 1234;

	public override void _Ready()
	{
		HexMetrics.InitializeHashGrid(seed);
		CreateMap(Width, Height);
	}

	public void CreateMap(int x, int z)
	{
		chunkCountWidth = x / HexMetrics.ChunkWidth;
		chunkCountHeight = z / HexMetrics.ChunkHeight;

		Width = x;
		Height = z;

		cellShaderData.Initialize(Width, Height);

		CreateChunks();
		CreateCells();
		Refresh();
	}

	public void Refresh()
	{
		foreach (var chunk in chunks)
		{
			chunk.Refresh();
		}

		foreach (var cell in cells)
		{
			cellShaderData.RefreshTerrain(cell);
		}
	}

	public HexCell GetCell(Coordinates coordinates)
	{
		int z = coordinates.Z;
		if (z < 0 || z >= Height)
		{
			return null;
		}

		int x = coordinates.X + z / 2;

		if (x < 0 || x >= Width)
		{
			return null;
		}

		return cells[x + z * Width];
	}

	public HexCell GetCell(Vector3 position)
	{
		Coordinates coords = Coordinates.FromWorld(position);
		return GetCell(coords.GetIndex(Width));
	}

	public HexCell GetCell(int index)
	{
		return cells[index];
	}

	private void CreateChunks()
	{
		chunks = new HexGridChunk[chunkCountWidth * chunkCountHeight];

		for (int z = 0, i = 0; z < chunkCountHeight; z++)
		{
			for (int x = 0; x < chunkCountWidth; x++)
			{
				HexGridChunk chunk = HexGridChunk.Instance();
				chunks[i] = chunk;
				AddChild(chunk);
				i++;
			}
		}
	}

	private void CreateCells()
	{
		cells = new HexCell[Width * Height];

		for (int z = 0, i = 0; z < Width; z++)
		{
			for (int x = 0; x < Height; x++)
			{
				CreateCell(x, z, i++);
			}
		}
	}

	private void CreateCell(int x, int z, int i)
	{
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.InnerRadius * 2f);
		position.y = 0f;
		position.z = z * HexMetrics.OuterRadius * 1.5f;

		HexCell cell = HexCell.Instance();

		cells[i] = cell;

		cell.Coordinates = Coordinates.FromOffsetCoordinates(x, z);
		cell.Index = i;
		cell.ShaderData = cellShaderData;
		cell.Translation = position;

		AddCellToChunk(cell, x, z);

		cell.Elevation = 0;

		if (x > 0)
		{
			cell.SetNeighbor(HexMetrics.Direction.W, cells[i - 1]);
		}

		if (z > 0)
		{
			if ((z & 1) == 0)
			{
				cell.SetNeighbor(HexMetrics.Direction.SE, cells[i - Width]);

				if (x > 0)
				{
					cell.SetNeighbor(HexMetrics.Direction.SW, cells[i - Width - 1]);
				}
			}
			else
			{
				cell.SetNeighbor(HexMetrics.Direction.SW, cells[i - Width]);

				if (x < Width - 1)
				{
					cell.SetNeighbor(HexMetrics.Direction.SE, cells[i - Width + 1]);
				}
			}
		}

	}

	public void FindDistancesTo(HexCell cell)
	{
		for (int i = 0; i < cells.Length; i++)
		{
			cells[i].Distance = cell.Coordinates.DistanceTo(cells[i].Coordinates);
		}
	}

	public void FindPath(HexCell fromCell, HexCell toCell, int moves)
	{
		ClearPath();

		currentPathFrom = fromCell;
		currentPathTo = toCell;

		currentPathExists = Search(fromCell, toCell, moves);

		if (currentPathExists)
		{
			ShowPath(moves);
		}
	}

	public bool Search(HexCell fromCell, HexCell toCell, int moves)
	{

		if (fromCell == toCell)
		{
			return false;
		}

		searchFrontierPhase += 2;

		if (searchFrontier == null)
		{
			searchFrontier = new HexCellPriorityQueue();
		}
		else
		{
			searchFrontier.Clear();
		}

		fromCell.EnableHighlight(new Color("0000FF"));
		fromCell.SearchPhase = searchFrontierPhase;
		fromCell.Distance = 0;
		searchFrontier.Enqueue(fromCell);

		while (searchFrontier.Count > 0)
		{
			HexCell current = searchFrontier.Dequeue();
			current.SearchPhase += 1;

			if (current == toCell)
			{
				return true;
			}

			int currentTurn = current.Distance / moves;

			for (HexMetrics.Direction d = HexMetrics.Direction.NE; d <= HexMetrics.Direction.NW; d++)
			{
				HexCell neighbor = current.GetNeighbor(d);


				if (neighbor == null || neighbor.SearchPhase > searchFrontierPhase)
				{
					continue;
				}

				if (current.GetEdgeType(neighbor) == HexMetrics.EdgeType.Cliff)
				{
					continue;
				}

				int moveCost = neighbor.MovementCost;

				int distance = current.Distance;
				int turn = distance / moves;

				distance += moveCost;

				if (turn > currentTurn)
				{
					distance = turn * moves + moveCost;
				}

				if (neighbor.SearchPhase < searchFrontierPhase)
				{
					neighbor.SearchPhase = searchFrontierPhase;
					neighbor.Distance = distance;
					neighbor.PathFrom = current;
					neighbor.SearchHeuristic = neighbor.Coordinates.DistanceTo(toCell.Coordinates);
					searchFrontier.Enqueue(neighbor);
				}
				else if (distance < neighbor.Distance)
				{
					int oldPriority = neighbor.SearchPriority;
					neighbor.Distance = distance;
					neighbor.PathFrom = current;
					searchFrontier.Change(neighbor, oldPriority);
				}

			}
		}

		return false;
	}

	public List<HexCell> GetVisibleCells(HexCell fromCell, int range)
	{
		List<HexCell> visibleCells = ListPool<HexCell>.Get();

		searchFrontierPhase += 2;

		if (searchFrontier == null)
		{
			searchFrontier = new HexCellPriorityQueue();
		}
		else
		{
			searchFrontier.Clear();
		}

		fromCell.SearchPhase = searchFrontierPhase;
		fromCell.Distance = 0;
		searchFrontier.Enqueue(fromCell);

		while (searchFrontier.Count > 0)
		{
			HexCell current = searchFrontier.Dequeue();
			current.SearchPhase += 1;

			visibleCells.Add(current);

			for (HexMetrics.Direction d = HexMetrics.Direction.NE; d <= HexMetrics.Direction.NW; d++)
			{
				HexCell neighbor = current.GetNeighbor(d);


				if (neighbor == null || neighbor.SearchPhase > searchFrontierPhase)
				{
					continue;
				}

				int distance = current.Distance + Math.Max(0, current.Elevation - fromCell.Elevation) + 1;

				if (distance > range)
				{
					continue;
				}

				if (neighbor.SearchPhase < searchFrontierPhase)
				{
					neighbor.SearchPhase = searchFrontierPhase;
					neighbor.Distance = distance;
					neighbor.SearchHeuristic = 0;
					searchFrontier.Enqueue(neighbor);
				}
				else if (distance < neighbor.Distance)
				{
					int oldPriority = neighbor.SearchPriority;
					neighbor.Distance = distance;
					searchFrontier.Change(neighbor, oldPriority);
				}

			}
		}

		return visibleCells;
	}

	public void IncreaseVisibility(HexCell fromCell, int range)
	{
		List<HexCell> cells = GetVisibleCells(fromCell, range);

		for (int i = 0; i < cells.Count; i++)
		{
			cells[i].IncreaseVisibility();
		}
		ListPool<HexCell>.Add(cells);
	}

	public void DecreaseVisibility(HexCell fromCell, int range)
	{
		List<HexCell> cells = GetVisibleCells(fromCell, range);

		for (int i = 0; i < cells.Count; i++)
		{
			cells[i].DecreaseVisibility();
		}
		ListPool<HexCell>.Add(cells);
	}

	public void ResetVisibility()
	{
		cellShaderData.ResetVisibility();
	}

	private void ShowPath(int speed)
	{
		if (currentPathExists)
		{
			HexCell current = currentPathTo;

			while (current != currentPathFrom)
			{
				int turn = current.Distance / speed;
				current.EnableHighlight(new Color("FFFFFF"));
				current = current.PathFrom;
			}
		}

		currentPathFrom.EnableHighlight(new Color("0000FF"));
		currentPathTo.EnableHighlight(new Color("FF0000"));
	}

	private void ClearPath()
	{
		if (currentPathExists)
		{
			HexCell current = currentPathTo;
			while (current != currentPathFrom)
			{
				current.DisableHighlight();
				current = current.PathFrom;
			}
			current.DisableHighlight();
			currentPathExists = false;
		}
		else if (currentPathFrom != null)
		{
			currentPathFrom.DisableHighlight();
			currentPathTo.DisableHighlight();
		}
		currentPathFrom = currentPathTo = null;
	}

	private void AddCellToChunk(HexCell cell, int x, int z)
	{
		int chunkX = x / HexMetrics.ChunkWidth;
		int chunkZ = z / HexMetrics.ChunkHeight;

		HexGridChunk chunk = chunks[chunkX + chunkZ * chunkCountWidth];

		int localX = x - chunkX * HexMetrics.ChunkWidth;
		int localZ = z - chunkZ * HexMetrics.ChunkHeight;

		int index = localZ * HexMetrics.ChunkWidth + localX;

		chunk.AddCell(index, cell);
	}
}
