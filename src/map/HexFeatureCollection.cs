using Godot;
using System;

public class HexFeatureCollection : Resource
{
    [Export] PackedScene[] scenes;

	public PackedScene Pick(float choice)
	{
		return scenes[(int)choice * scenes.Length];
	}
}
