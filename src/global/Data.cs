using Godot;
using System;

public class Data : Node
{
	[Export] StreamTexture[] textures;

    public override void _Ready()
    {
    }

	public TextureArray GetTextureArray()
	{
		TextureArray array = new TextureArray();
		array.Create(512, 512, (uint)textures.Length, Image.Format.Rgb8, 7);

		for (int i = 0; i < textures.Length; i++)
		{
			array.SetLayerData(textures[i].GetData(), i);
		}
		return array;
	}

	public Texture[] GetTextures()
	{
		return textures;
	}
}
