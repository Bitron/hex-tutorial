extends Spatial

export var speed := 10.0
export var rotation_speed := 50.0

onready var camera = $Camera

func _process(delta):
	if Input.is_action_pressed("camera_left"):
		translate(Vector3.LEFT * speed * delta)

	if Input.is_action_pressed("camera_right"):
		translate(Vector3.RIGHT * speed * delta)

	if Input.is_action_pressed("camera_forward"):
		translate(Vector3.FORWARD * speed * delta)

	if Input.is_action_pressed("camera_back"):
		translate(Vector3.BACK * speed * delta)

	if Input.is_action_pressed("camera_up"):
		translate(Vector3.UP * speed * delta)

	if Input.is_action_pressed("camera_down"):
		translate(Vector3.DOWN * speed * delta)

	if Input.is_action_pressed("camera_rotate_left"):
		rotation_degrees.y += rotation_speed * delta

	if Input.is_action_pressed("camera_rotate_right"):
		rotation_degrees.y -= rotation_speed * delta

	if Input.is_action_pressed("ui_up"):
		camera.rotation_degrees.x += rotation_speed * delta;

	if Input.is_action_pressed("ui_down"):
		camera.rotation_degrees.x -= rotation_speed * delta

	camera.rotation_degrees.x = clamp(camera.rotation_degrees.x, -90, 90)
