using Godot;
using System;

public class HexCellShaderData : Godot.Object
{
	Image cellImage;
	ImageTexture cellTexture;
	Color[] cellTextureData;

	private Material terrainMaterial = GD.Load<Material>("res://graphics/materials/terrain_mat.tres");
	private Material roadMaterial = GD.Load<Material>("res://graphics/materials/roads_mat.tres");
	private Material waterMaterial = GD.Load<Material>("res://graphics/materials/water_mat.tres");

	bool enabled = false;

	public void Initialize(int x, int z)
	{
		if (cellImage != null)
		{
			cellImage.Resize(x, z);
		}
		else
		{
			cellImage = new Image();
			cellImage.Create(x, z, true, Image.Format.Rgba8);
			cellTexture = new ImageTexture();
			cellTexture.CreateFromImage(cellImage, 1);
			terrainMaterial.Set("shader_param/cell_texture", cellTexture);
			roadMaterial.Set("shader_param/cell_texture", cellTexture);
			waterMaterial.Set("shader_param/cell_texture", cellTexture);
		}

		Vector2 uv = new Vector2(1f / x, 1f / z);
		terrainMaterial.Set("shader_param/texel_size", uv);
		roadMaterial.Set("shader_param/texel_size", uv);
		waterMaterial.Set("shader_param/texel_size", uv);


		if (cellTextureData == null || cellTextureData.Length != x * z)
		{
			cellTextureData = new Color[x * z];
		}
		else
		{
			for (int i = 0; i < cellTextureData.Length; i++)
			{
				cellTextureData[i] = new Color(0f, 0f, 0f, 0f);
				// GD.Print("Set Texture Data: ", i, cellTextureData[i]);
			}
		}

		enabled = true;
		CallDeferred("Apply");
	}

	public void RefreshTerrain (HexCell cell)
	{
		cellTextureData[cell.Index].a8 = cell.TerrainTypeIndex;
		// GD.Print("Index: ", cell.Index, " Type: ", cell.TerrainTypeIndex);

		enabled = true;
		CallDeferred("Apply");
	}

	public void RefreshVisibility (HexCell cell) {
		cellTextureData[cell.Index].r8 = cell.IsVisible ? 255 : 0;
		
		enabled = true;
		CallDeferred("Apply");
	}

	public void ResetVisibility()
	{
		for (int i = 0; i < cellTextureData.Length; i++)
		{
			cellTextureData[i].r8 = 0;		
		}

		enabled = true;
		CallDeferred("Apply");
	}

	public Texture GetTexture()
	{
		cellImage.Lock();
		
		for (int i = 0; i < cellTextureData.Length; i++)
		{
			int x = i % cellImage.GetWidth();
			int z = i / cellImage.GetHeight();

			var c = cellTextureData[i];
			cellImage.SetPixel(x, z, c);
			
			// GD.Print("Set Pixel, (", x, ", ", z, ") ", c.a * 255f);			
		}
		cellImage.Unlock();

		cellTexture.CreateFromImage(cellImage, 1);
		return cellTexture;
	}

	public void Apply()
	{
		if (!enabled)
		{
			return;
		}
		
		enabled = false;
		
		terrainMaterial.Set("shader_param/cell_texture", GetTexture());
		roadMaterial.Set("shader_param/cell_texture", GetTexture());
		waterMaterial.Set("shader_param/cell_texture", GetTexture());
	}
}
