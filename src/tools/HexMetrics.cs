using Godot;
using System;

public struct HexHash
{
	public float a, b, c, d, e, f;

	public static HexHash Create()
	{
		HexHash hash;
		hash.a = GD.Randf() * 0.999f;
		hash.b = GD.Randf() * 0.999f;
		hash.c = GD.Randf() * 0.999f;
		hash.d = GD.Randf() * 0.999f;
		hash.e = GD.Randf() * 0.999f;
		hash.f = GD.Randf() * 0.999f;
		return hash;
	}
}

public static class HexMetrics
{
	public enum Direction { NE, E, SE, SW, W, NW };
	public enum EdgeType { Flat, Slope, Cliff }


	public const int ChunkWidth = 5;
	public const int ChunkHeight = 5;

	public const float OuterToInner = 0.866025404f;
	public const float InnerToOuter = 1f / OuterToInner;
	public const float OuterRadius = 10f;
	public const float InnerRadius = OuterRadius * OuterToInner;

	public const float SolidFactor = 0.75f;
	public const float BlendFactor = 1f - SolidFactor;

	public const float ElevationStep = 2.5f;

	public const int TerracesPerSlope = 2;
	public const int TerraceSteps = TerracesPerSlope * 2 + 1;
	public const float HorizontalTerraceStepSize = 1f / TerraceSteps;
	public const float VerticalTerraceStepSize = 1f / (TerracesPerSlope + 1);

	public const float WallHeight = 3.5f;
	public const float WallThickness = 0.75f;
	public const float WallElevationOffset = VerticalTerraceStepSize;

	public const float StreamBedElevationOffset = -1.75f;
	public const float WaterElevationOffset = -0.3f;
	public const float WaterFactor = 0.6f;
	public const float WaterBlendFactor = 1f - WaterFactor;

	public const float CellPerturbStrength = 2.5f;
	public const float ElevationPerturpStrength = 0.75f;

	public const float NoiseScale = 1f;
	public static Image Noise = GD.Load<Texture>("res://graphics/images/noise.png").GetData();

	public const int HashGridSize = 256;
	public const float HashGridScale = 0.25f;

	private static HexHash[] hashGrid;

	public static Vector3[] corners = {
		new Vector3(0f, 0f, OuterRadius),
		new Vector3(InnerRadius, 0f, 0.5f * OuterRadius),
		new Vector3(InnerRadius, 0f, -0.5f * OuterRadius),
		new Vector3(0f, 0f, -OuterRadius),
		new Vector3(-InnerRadius, 0f, -0.5f * OuterRadius),
		new Vector3(-InnerRadius, 0f, 0.5f * OuterRadius),
		new Vector3(0f, 0f, OuterRadius),
	};

	static float[][] FeatureThresholds = {
		new float[] {0.0f, 0.0f, 0.4f},
		new float[] {0.0f, 0.4f, 0.6f},
		new float[] {0.4f, 0.6f, 0.8f}
	};

	public static float[] GetFeatureThresholds(int level)
	{
		return FeatureThresholds[level];
	}

	public static void InitializeHashGrid(int seed)
	{
		GD.Seed((ulong)seed);

		hashGrid = new HexHash[HashGridSize * HashGridSize];
		for (int i = 0; i < hashGrid.Length; i++)
		{
			hashGrid[i] = HexHash.Create();
		}
	}

	public static Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
	{
		float h = step * HorizontalTerraceStepSize;

		a.x += (b.x - a.x) * h;
		a.z += (b.z - a.z) * h;

		float v = ((step + 1) / 2) * VerticalTerraceStepSize;

		a.y += (b.y - a.y) * v;

		return a;
	}

	public static Color TerraceLerp(Color a, Color b, int step)
	{
		float h = step * HorizontalTerraceStepSize;
		return a.LinearInterpolate(b, h);
	}

	public static Vector3 WallLerp(Vector3 near, Vector3 far)
	{
		near.x += (far.x - near.x) * 0.5f;
		near.z += (far.z - near.z) * 0.5f;
		float v =
			near.y < far.y ? WallElevationOffset : (1f - WallElevationOffset);
		near.y += (far.y - near.y) * v;
		return near;
	}

	public static Vector3 WallThicknessOffset(Vector3 near, Vector3 far)
	{
		Vector3 offset;
		offset.x = far.x - near.x;
		offset.y = 0f;
		offset.z = far.z - near.z;
		return offset.Normalized() * (WallThickness * 0.5f); ;
	}

	public static Color SampleNoise(Vector3 position)
	{
		Noise.Lock();
		int x = (int)(position.x * NoiseScale) % 512;
		int z = (int)(position.z * NoiseScale) % 512;
		return Noise.GetPixel(Mathf.Abs(x), Mathf.Abs(z));
	}

	public static HexHash SampleHashGrid(Vector3 position)
	{
		int x = (int)(position.x * HashGridScale) % HashGridSize;

		if (x < 0)
		{
			x += HashGridSize;
		}

		int z = (int)(position.z * HashGridScale) % HashGridSize;

		if (z < 0)
		{
			z += HashGridSize;
		}

		return hashGrid[x + z * HashGridSize];
	}

	public static Vector3 Perturb(Vector3 position)
	{
		Color sample = SampleNoise(position);

		position.x += (sample.r * 2f - 1f) * CellPerturbStrength;
		position.z += (sample.b * 2f - 1f) * CellPerturbStrength;

		return position;
	}

	public static Vector3 GetBridge(HexMetrics.Direction direction)
	{
		return (corners[(int)direction] + corners[(int)direction + 1]) * BlendFactor;
	}

	public static Vector3 GetFirstCorner(Direction direction)
	{
		return corners[(int)direction];
	}

	public static Vector3 GetSecondCorner(Direction direction)
	{
		return corners[(int)direction + 1];
	}

	public static Vector3 GetFirstSolidCorner(Direction direction)
	{
		return corners[(int)direction] * SolidFactor;
	}

	public static Vector3 GetSecondSolidCorner(Direction direction)
	{
		return corners[(int)direction + 1] * SolidFactor;
	}

	public static Vector3 GetSolidEdgeMiddle(HexMetrics.Direction direction)
	{
		return (corners[(int)direction] + corners[(int)direction + 1]) * (0.5f * SolidFactor);
	}

	public static Vector3 GetWaterBridge(HexMetrics.Direction direction)
	{
		return (corners[(int)direction] + corners[(int)direction + 1]) * WaterBlendFactor;
	}

	public static Vector3 GetFirstWaterCorner(HexMetrics.Direction direction)
	{
		return corners[(int)direction] * WaterFactor;
	}

	public static Vector3 GetSecondWaterCorner(HexMetrics.Direction direction)
	{
		return corners[(int)direction + 1] * WaterFactor;
	}

	public static EdgeType GetEdgeType(int elevation1, int elevation2)
	{
		if (elevation1 == elevation2)
		{
			return EdgeType.Flat;
		}

		int delta = elevation2 - elevation1;

		if (delta == 1 || delta == -1)
		{
			return EdgeType.Slope;
		}

		return EdgeType.Cliff;
	}
}

public static class DirectionExtentions
{
	public static HexMetrics.Direction Opposite(this HexMetrics.Direction direction)
	{
		return (int)direction < 3 ? (direction + 3) : (direction - 3);

	}

	public static HexMetrics.Direction Previous(this HexMetrics.Direction direction)
	{
		return direction == HexMetrics.Direction.NE ? HexMetrics.Direction.NW : (direction - 1);
	}

	public static HexMetrics.Direction Next(this HexMetrics.Direction direction)
	{
		return direction == HexMetrics.Direction.NW ? HexMetrics.Direction.NE : (direction + 1);
	}

	public static HexMetrics.Direction Previous2(this HexMetrics.Direction direction)
	{
		direction -= 2;
		return direction >= HexMetrics.Direction.NE ? direction : (direction + 6);
	}

	public static HexMetrics.Direction Next2(this HexMetrics.Direction direction)
	{
		direction += 2;
		return direction <= HexMetrics.Direction.NW ? direction : (direction - 6);
	}
}